# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kompare
pkgver=20.08.3
pkgrel=1
arch="all !armhf !s390x !mips64"
url="https://kde.org/applications/development/org.kde.kompare"
pkgdesc="Graphical File Differences Tool"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="extra-cmake-modules qt5-qtbase-dev kcoreaddons-dev kcodecs-dev kdoctools-dev kiconthemes-dev kjobwidgets-dev kconfig-dev kparts-dev ktexteditor-dev kwidgetsaddons-dev libkomparediff2-dev"
source="https://download.kde.org/stable/release-service/$pkgver/src/kompare-$pkgver.tar.xz
	Portaway-from-Obsolete-methods-of-QPrinter.patch
	"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="0cb169296924e4dad2967052472fd7095092478819ba0925d82482f0e279719b2e29e75db9e732602cd845261918025ae9cf0e024c8ad0a250c04abe022deb4b  kompare-20.08.3.tar.xz
dd1b953af8be55e935dc32b156f020aed77c081ee9f37916ceaf1215f34c1aeb921c883045fae4c4c85fccdac1e734cb7c19344f1440e91d905ebdeadb29ca1e  Portaway-from-Obsolete-methods-of-QPrinter.patch"
